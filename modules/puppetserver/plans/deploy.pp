plan puppetserver::deploy(
  String[1]  $installer_tarball,
  String[1]  $installer_tarball_remote  ='/tmp/installer.tar.gz',
  String[1]  $console_admin_password    ='puppet',
  TargetSpec $nodes,
) {
  file_upload($installer_tarball, $installer_tarball_remote, $nodes, '_run_as' => 'root')
  return run_task('puppetserver::install', $nodes, {
      '_run_as'                => 'root',
      'installer_tarball'      => $installer_tarball_remote,
    }
  )
}
