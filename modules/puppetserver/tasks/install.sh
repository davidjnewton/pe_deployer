#!/bin/sh

# Puppet Task Name: 
#
# This is where you put the shell code for your task.
#
# You can write Puppet tasks in any language you want and it's easy to
# adapt an existing Python, PowerShell, Ruby, etc. script. Learn more at:
# https://puppet.com/docs/bolt/0.x/writing_tasks.html
#
# Puppet tasks make it easy for you to enable others to use your script. Tasks
# describe what it does, explains parameters and which are required or optional,
# as well as validates parameter type. For examples, if parameter "instances"
# must be an integer and the optional "datacenter" parameter must be one of
# portland, sydney, belfast or singapore then the .json file
# would include:
#   "parameters": {
#     "instances": {
#       "description": "Number of instances to create",
#       "type": "Integer"
#     },
#     "datacenter": {
#       "description": "Datacenter where instances will be created",
#       "type": "Enum[portland, sydney, belfast, singapore]"
#     }
#   }
# Learn more at: https://puppet.com/docs/bolt/0.x/writing_tasks.html#ariaid-title11
#

{ # try:

tmp=/tmp/puppetserver$$
rm -rf $tmp
mkdir -p $tmp
sudo tar -zxvf $PT_installer_tarball -C $tmp --strip-components=1
if [ -z "$PT_pe_conf"]; then
  PT_pe_conf="${tmp}/pe.conf"
  cat > $PT_pe_conf <<- EOF
  {
    "console_admin_password": "puppet"
    "puppet_enterprise::puppet_master_host": "%{::trusted.certname}"
  }
EOF
fi
sudo $tmp/puppet-enterprise-installer -c $PT_pe_conf

} || { # catch:

  echo "Installation failed."

}

# finally:

rm -rf $tmp
rm -rf $PT_installer_tarball